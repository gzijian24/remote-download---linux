using LinuxHelpSln.Models;
using Microsoft.AspNetCore.Mvc;
using Renci.SshNet;
using System.Diagnostics;
using System.Net.Http.Headers;
using System.Text;

namespace LinuxHelpSln.Controllers
{
    public class LinuxFileController : Controller
    {
        private readonly ILogger<LinuxFileController> _logger;

        public LinuxFileController(ILogger<LinuxFileController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// 获取linux服务器文件夹内容
        /// </summary>
        /// <param name="host">源linux服务器IP</param>
        /// <param name="username">源linux服务器登录账号/param>
        /// <param name="password">源linux服务器登录密码</param>
        /// <param name="downLoadPath">源linux服务器下载文件夹</param>
        /// <returns></returns>
        public IActionResult GetLinuxFileList(string host, string username, string password, string downLoadPath)
        {
            var myObj = GetFileList(host, username, password, downLoadPath).Distinct();
            return new ObjectResult(myObj);
        }


        public List<string> GetFileList(string host, string username, string password, string downLoadPath)
        {
            List<string> fileList = new List<string>();
            using (var client = new SshClient(host, username, password))
            {
                client.Connect();
                var command = client.CreateCommand($"ls {downLoadPath}");
                var results = command.Execute();
                foreach (var line in results.Split('\n'))
                {
                    if (line.EndsWith(":"))
                    {
                        // 文件夹
                        string folderName = line.Replace(":", "");
                        command = client.CreateCommand($"ls {downLoadPath}/{folderName}");
                        var subResults = command.Execute();
                        foreach (var subLine in subResults.Split('\n'))
                        {
                            if (!string.IsNullOrEmpty(subLine))
                            {
                                fileList.Add($"{downLoadPath}/{folderName}/{subLine}");
                            }
                        }
                    }
                    else
                    {
                        // 文件
                        fileList.Add($"{downLoadPath}/{line}");
                    }


                    fileList.Add($"{downLoadPath}/{line}");
                }
                client.Disconnect();
            }
            return fileList;
        }

        /// <summary>
        /// 下载单个linux文件至本地
        /// </summary>
        /// <param name="host">源linux服务器IP</param>
        /// <param name="username">源linux服务器登录账号/param>
        /// <param name="password">源linux服务器登录密码</param>
        /// <param name="downLoadPath">源linux服务器下载文件路径</param>
        /// <param name="localPath">本地文件保存路径</param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult DownloadFile(string host, string username, string password, string downLoadPath, string localPath)
        {
            try
            {
                // Define the local download directory.
                string downloadDirectory = localPath;
                if (!Directory.Exists(localPath))
                {
                    Directory.CreateDirectory(localPath);
                }
                // Create the local file path.
                string localFilePath = Path.Combine(downloadDirectory, Path.GetFileName(downLoadPath));

                // Connect to the SFTP server and download the file.
                using (var client = new SftpClient(host, 22, username, password))
                {
                    client.Connect();
                    using (var fileStream = new FileStream(localFilePath, FileMode.Create))
                    {
                        client.DownloadFile(downLoadPath, fileStream);
                    }
                    client.Disconnect();
                }

                // Return the downloaded file to the user for download.
                return File(new FileStream(localFilePath, FileMode.Open), "application/octet-stream", Path.GetFileName(downLoadPath));
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        /// <summary>
        /// 下载linux文件夹内所有文件至本地
        /// </summary>
        /// <param name="host">源linux服务器IP</param>
        /// <param name="username">源linux服务器登录账号/param>
        /// <param name="password">源linux服务器登录密码</param>
        /// <param name="downLoadPath">源linux服务器下载文件夹</param>
        /// <param name="localPath">本地文件保存路径</param>
        /// <returns></returns>
        [HttpGet]
        public void DownloadAllFile(string host, string username, string password, string downLoadAllPath, string localPath)
        {

            // 设置本地保存目录.
            string downloadDirectory = localPath;
            if (!Directory.Exists(localPath))
            {
                Directory.CreateDirectory(localPath);
            }
            //根据源Linux文件夹遍历文件夹文件
            List<string> downLoadAllPathList = GetFileList(host, username, password, downLoadAllPath);
            foreach (var downLoadPath in downLoadAllPathList)
            {
                try
                {
                    string localFilePath = Path.Combine(downloadDirectory, Path.GetFileName(downLoadPath));

                    // Connect to the SFTP server and download the file.
                    using (var client = new SftpClient(host, 22, username, password))
                    {
                        client.Connect();
                        using (var fileStream = new FileStream(localFilePath, FileMode.Create))
                        {
                            client.DownloadFile(downLoadPath, fileStream);
                        }
                        client.Disconnect();
                    }

                    // Return the downloaded file to the user for download.
                    File(new FileStream(localFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite), "application/octet-stream", Path.GetFileName(downLoadPath));
                }
                catch (Exception)
                {
                }
            }
        }


    }
}